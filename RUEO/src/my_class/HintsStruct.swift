import Foundation

struct HintsStruct: Decodable {
    var value: String
    
}

extension Array where Element == HintsStruct {
    func getValueArray() -> [String]{
        return self.map { $0.value }
    }
}

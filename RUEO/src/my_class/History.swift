import Foundation
import Realm
import RealmSwift

class History{

    static let shared = History()
    let maxCountHistory = 30
    
    var history: [String] = []
    
    let listForNavigation: DoublyLinkedList!
    
    private init() {
        let realm = try! Realm()
        if let historyFromRealm = realm.objects(HistoryModel.self).first {
            history = historyFromRealm.historyList.map { $0.item }
        }
        listForNavigation = DoublyLinkedList(items: history)
    }
    
    func add(item: String){
        history = history.filter { $0 != item }
        history.append(item)
        
        if history.count > maxCountHistory {
            let removeString = history.removeFirst()
            listForNavigation.remove(item: removeString)
        }
        listForNavigation.add(item: item)
        save()
    }
    
    func next() {
        listForNavigation.next()
    }
    func prev(){
        listForNavigation.prev()
    }
    
    func getCurrentWord() -> String{
        return listForNavigation.currentItem?.value ?? "EMPTY HISTORY!!"
    }
    
    func removeAll(){
        history.removeAll()
        listForNavigation.removeAll()
    }
    
    func setCurrent(word: String){
        if listForNavigation.isMember(value: word){
            listForNavigation.setCurrent(word: word)
        }else{
            add(item: word)
        }
    }
    
    func save(){
        let realm = try! Realm()
        let historyForRealm = HistoryModel()
        let realmHistory = List<HistoryItemModel>()
        
        history.forEach {str in
            let item = HistoryItemModel()
            item.item = str
            realmHistory.append(item)
        }
        
        historyForRealm.historyList = realmHistory
        
        try! realm.write {
            realm.add(historyForRealm)
        }
    }

    deinit {
        save()
    }
}


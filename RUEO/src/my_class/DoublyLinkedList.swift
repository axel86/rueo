import Foundation

class HistoryItem{
    let value: String!
    var next: HistoryItem?
    var prev: HistoryItem?
    init(value: String){
        self.value = value
    }
    
    static func == (left: HistoryItem, right: HistoryItem) -> Bool{
        return left.value == right.value
    }
}

class DoublyLinkedList{
    var listHead: HistoryItem?
    var listTail: HistoryItem?
    var currentItem: HistoryItem?
    
    init(items: [String]){
        for item in items{
            self.add(item: item)
        }
    }
    
    func add(item: String){
        let newItem = HistoryItem(value: item)
        remove()
        if listHead == nil, listTail == nil{
            listHead = newItem
            listTail = newItem
        } else {
            let tail = listTail
            newItem.prev = tail
            tail?.next = newItem
            listTail = newItem
        }
        currentItem = newItem
    }
    
    func next(){
        guard let _ = currentItem?.next else { return }
        currentItem = currentItem?.next
    }
    func prev(){
        guard let _ = currentItem?.prev else { return }
        currentItem = currentItem?.prev
    }
    
    func remove() {
        guard let current = currentItem,
            let tail = listTail else { return }
        if !(current == tail){
            listTail = tail.prev
            tail.prev?.next = nil
            tail.prev = nil
            remove()
        }
    }
    
    func remove(item: String){
        if listHead?.value == item{
            let head = listHead
            listHead = listHead?.next
            listHead?.prev = nil
            head?.next = nil
        }
        if listTail?.value == item{
            let tail = listTail
            listTail = listTail?.prev
            listTail?.next = nil
            tail?.prev = nil
        }
        var current = listHead
        while (current != nil){
            if current?.value == item {
                if current?.value == currentItem?.value{
                    if currentItem?.next != nil {
                        self.next()
                    }else {
                        self.prev()
                    }
                }
                let next = current?.next
                let prev = current?.prev
                
                next?.prev = prev
                prev?.next = next
                
                currentItem?.next = nil
                currentItem?.prev = nil
            }
            current = current?.next
        }
    }
    
    func removeAll(){
        var current = listHead
        listHead = nil
        listTail = nil
        currentItem = nil
        while (current != nil){
            current?.prev?.next = nil
            current?.prev = nil
            current = current?.next
        }
    }
    
    func isMember(value: String) -> Bool {
        var current = listHead
        while (current != nil && current?.value != value){
            current = current?.next
        }
        return current?.value == value
    }
    
    func setCurrent(word: String){
        if currentItem?.value == word { return }
    
        var next = currentItem?.next
        var prev = currentItem?.prev
        
        while (next?.value != word && prev?.value != word || (next == nil && prev == nil)){
            next = next?.next
            prev = prev?.prev
        }
        if next != nil{
            currentItem = next
        }
        if prev != nil{
            currentItem = prev
        }
    }
    
}

import UIKit

class AnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    var isPresentation: Bool = false
    var direction: CGFloat = 1
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) ,
            let toVC = transitionContext.viewController(forKey: .to) else
        {return}
        let fromView = fromVC.view!
        let toView = toVC.view!
        let containerView = transitionContext.containerView
        
        if isPresentation{
            containerView.addSubview(toView)
        }
        
        let animatingVC = isPresentation ? toVC : fromVC
        let animatingView = animatingVC.view!
        
        let finalFrameForVC = transitionContext.finalFrame(for: animatingVC)
        var initialFrameForVC = finalFrameForVC
        
        let newX = direction * (isPresentation ? (containerView.frame.width * 0.4) : (containerView.frame.width * 0.6))
        initialFrameForVC.origin.x += newX
        
        let initialFrame = isPresentation ? initialFrameForVC : finalFrameForVC
        let finalFrame = isPresentation ? finalFrameForVC : initialFrameForVC
        
        animatingView.frame = initialFrame
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       usingSpringWithDamping: 300.0,
                       initialSpringVelocity: 5.0,
                       options: UIViewAnimationOptions.allowUserInteraction ,
                       animations: {
                        animatingView.frame = finalFrame
        }, completion: {(value: Bool) in
            if !self.isPresentation{
                fromView.removeFromSuperview()
            }
            transitionContext.completeTransition(true)
        })
    }
}

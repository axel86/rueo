import UIKit

class PresentationController: UIPresentationController, UIAdaptivePresentationControllerDelegate {
    
    var chromeView: UIView! = UIView()
    
    let direction: CGFloat
    
    init(presentedViewController: UIViewController, presentingViewController: UIViewController?, direction: CGFloat) {
        self.direction = direction
        chromeView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        chromeView.alpha = 0.0
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.chromeViewTapped(gesture:)))
        chromeView.addGestureRecognizer(tap)
    }
    
    override var frameOfPresentedViewInContainerView: CGRect{
        var presentedViewFrame = CGRect.zero
        guard let containerView = containerView else {
            return presentedViewFrame
        }
        let containerBounds = containerView.bounds
        presentedViewFrame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
        presentedViewFrame.origin.x = (direction == 1) ? containerView.frame.size.width * 0.4 : 0
        
        
        return presentedViewFrame
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSize(width: parentSize.width * 0.6, height: parentSize.height )
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        guard let containerView = self.containerView else {return}
        chromeView.frame = containerView.bounds
        chromeView.alpha = 0.0
        containerView.insertSubview(chromeView, at: 0)
        if let coordinator = presentedViewController.transitionCoordinator{
            coordinator.animate(alongsideTransition: {
                (context: UIViewControllerTransitionCoordinatorContext) -> Void in
                self.chromeView.alpha = 1.0
            }, completion: nil)
        }else{
            chromeView.alpha = 1.0
        }
        
    }
    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        if let coordinator = presentedViewController.transitionCoordinator {
            coordinator.animate(alongsideTransition: {
                (context: UIViewControllerTransitionCoordinatorContext) -> Void in
                self.chromeView.alpha = 0.0
            }, completion: nil)
        }else{
            chromeView.alpha = 0.0
        }
    }
    override func containerViewWillLayoutSubviews() {
        guard let containerView = self.containerView else {return}
        chromeView.frame = containerView.bounds
        presentedView!.frame = frameOfPresentedViewInContainerView
    }
    override var shouldPresentInFullscreen: Bool{
        return true
    }
    
    override var adaptivePresentationStyle: UIModalPresentationStyle{
        return UIModalPresentationStyle.fullScreen
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.overFullScreen
    }
    @objc func chromeViewTapped(gesture: UIGestureRecognizer){
        if (gesture.state == UIGestureRecognizerState.ended){
            presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
}

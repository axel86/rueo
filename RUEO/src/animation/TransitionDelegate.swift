import UIKit

enum DirectionAnimation: CGFloat {
    case fromLeft = 1
    case fromRight = -1
}

class TransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    static let fromLeft = TransitionDelegate(to: .fromLeft)
    static let fromRight = TransitionDelegate(to: .fromRight)
    
    private var direction: CGFloat!
    
    private init(to direction: DirectionAnimation) {
        super.init()
        
        self.direction = direction.rawValue
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = PresentationController(presentedViewController: presented, presentingViewController: presenting, direction: direction)
        
        return presentationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = AnimatedTransition()
        animationController.isPresentation = true
        animationController.direction = self.direction
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = AnimatedTransition()
        animationController.isPresentation = false
        animationController.direction = self.direction
        return animationController
    }
}

import UIKit
import Foundation

protocol HistoryTableDelegate: class {
    func onTap(word: String)
}

class HistoryViewController: UITableViewController {
    weak var delegate: HistoryTableDelegate?
    
    var cellDescriptions: [TableViewCellDescription] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDescriptions()
        setupTable()
    }
    
    func setupDescriptions(){
        let history = History.shared.history
        cellDescriptions = []
        for item in history{
            cellDescriptions.append(
                TableViewCellDescription(cellType: HistoryTableCell.self,
                                         height: UITableViewAutomaticDimension,
                                         object: HistoryTableCellStruct(title: item)
                )
            )
        }
    }
    
    func setupTable(){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 35
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellDescriptions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.configureCell(with: cellDescriptions[indexPath.row], for: indexPath)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellDescriptions[indexPath.row].height
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HistoryTableHeader(frame: CGRect.zero)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = HistoryTableFooter(frame: CGRect.zero)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapClear))
        footer.view.addGestureRecognizer(tap)
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return HistoryTableFooter.historyTableFooterHeight
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HistoryTableHeader.historyTableHeaderHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let historyCell = tableView.cellForRow(at: indexPath) as? HistoryTableCell else { return }
        let word = historyCell.getWord()
        
        delegate?.onTap(word: word)
        History.shared.setCurrent(word: word)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onTapClear(){
        History.shared.removeAll()
        setupDescriptions()
        tableView.reloadData()
    }
}

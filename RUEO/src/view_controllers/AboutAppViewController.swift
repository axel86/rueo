import Foundation
import UIKit

class AboutAppViewController: UIViewController{
    @IBOutlet weak var stackToTopSuperView: NSLayoutConstraint!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        stackToTopSuperView.constant = NSLayoutConstraint.lenghtToSuperView
    }
}

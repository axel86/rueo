import UIKit

enum TypeWindow{
    case search
    case hint
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var fieldForWriteWord: UITextField!{
        didSet{
            fieldForWriteWord.delegate = self
            fieldForWriteWord.makeDefaultTextField(with: "Введите слово!")
        }
    }
    @IBOutlet weak var stackToTopSuperView: NSLayoutConstraint!
    @IBOutlet weak var heightSearchBar: NSLayoutConstraint!
    
    private var typeWindow: TypeWindow = .hint
    private var currentWord: String = ""
    private var currentWordForHint: String = ""
    private let hintTable: HintTable = HintTable()
    private let resultSearch: ResultSearch = ResultSearch()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        stackToTopSuperView.constant = NSLayoutConstraint.lenghtToSuperView
        heightSearchBar.constant = NSLayoutConstraint.heightForSearchBar
    }
    
    func setupView(){
        self.contentView.setContent(hintTable)
        
        hintTable.delegate = self
        resultSearch.delegate = self
    }
    
    func changeView(with word: String) {
        switch typeWindow {
        case .hint:
            resultSearch.removeFromSuperview()
            contentView.setContent(hintTable)
            hintTable.goHintRequest(with: word)
            currentWordForHint = word
        case .search:
            hintTable.removeFromSuperview()
            contentView.setContent(resultSearch)
            resultSearch.load(word: word)
            setDefaultColorSearchField()
            view.endEditing(true)
        }
    }
    
    func onSearch(for word: String, type: TypeWindow){
        currentWord = word
        typeWindow = type
        changeView(with: word)
    }
    
    func setAndRequest(word: String) {
        fieldForWriteWord.resignFirstResponder()
        fieldForWriteWord.text = word
        currentWordForHint = word
        onSearch(for: word, type: .search)
    }
    
    @IBAction func onTapHistory(_ sender: Any) {
        guard let vc = getViewController(in: Constants.NameStoryboard.history, with: TransitionDelegate.fromLeft) as? HistoryViewController else { return }
        
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onTapMenu(_ sender: Any){
        let vc = getViewController(in: Constants.NameStoryboard.aboutApp, with: TransitionDelegate.fromRight)
        
        present(vc, animated: true, completion: nil)
    }
    
    func getViewController(in storyboardName: String, with transitioningDelegate: TransitionDelegate) -> UIViewController{
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        guard let vc = storyboard.instantiateInitialViewController() else { return UIViewController() }
        vc.transitioningDelegate = transitioningDelegate
        self.transitioningDelegate = transitioningDelegate
        vc.modalPresentationStyle = .custom
        
        return vc
    }

    @IBAction func onTapClear(_ sender: Any) {
        fieldForWriteWord.text = ""
        currentWordForHint = ""
        onSearch(for: "", type: .hint)
    }
    @IBAction func onTapPrev(_ sender: Any) {
        History.shared.prev()
        setAndRequest(word: History.shared.getCurrentWord())
    }
    @IBAction func onTapNext(_ sender: Any) {
        History.shared.next()
        setAndRequest(word: History.shared.getCurrentWord())
    }
    
    @IBAction func swipe(recognizer: UIScreenEdgePanGestureRecognizer){
        print(recognizer.edges)
    }
}

extension MainViewController: ResultSearchDelegate {
    func setRedColorSearchField() {
        if !currentWordForHint.isEmpty{
            currentWordForHint.removeLast()
            
            fieldForWriteWord.textColor = UIColor.red
            
            typeWindow = .hint
            
            changeView(with: currentWordForHint)
        }
    }
    
    func setDefaultColorSearchField() {
        fieldForWriteWord.textColor = UIColor.white
    }
    
    func showAlert(alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
}

extension MainViewController: HintTableDelegate {
    
    // MARK: посмотреть изменение цвета
    func isEqualCurrentWord(for word: String) {
        print(currentWord)
        print(currentWordForHint)
        if currentWord != currentWordForHint {
            fieldForWriteWord.textColor = UIColor.red
        } else {
            fieldForWriteWord.textColor = UIColor.white
        }
    }
    
    func repeatRemoveLastChar(with word: String) {
        setRedColorSearchField()
    }

}

extension MainViewController: UITextFieldDelegate {

    @IBAction func editing(_ sender: Any) {
        guard let word = fieldForWriteWord.text else { return }
        if typeWindow == .search {
            fieldForWriteWord.text = ""
        }
        onSearch(for: word, type: .hint)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fieldForWriteWord.resignFirstResponder()
        if let text = fieldForWriteWord.text, !text.isEmpty {
            onSearch(for: text, type: .search)
            History.shared.add(item: text)
        }
        return false
    }

}

extension MainViewController: HistoryTableDelegate{
    func onTap(word: String) {
        setAndRequest(word: word)
    }
}

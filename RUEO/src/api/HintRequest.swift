import Foundation
import UIKit

class HintRequest: BaseRequest {

    
    func request(with word: String, _ callback: @escaping([String]?, Error?) -> Void) {
        urlComponents.queryItems = [
            URLQueryItem(name: "ajax", value: nil),
            URLQueryItem(name: "term", value: word)
        ]
        guard let url = urlComponents.url else {
            callback(nil, nil)
            return 
        }
        
        currentTask?.cancel()
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: TimeInterval(5))
        request.httpMethod = "GET"
        
        self.currentTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error,
                data == nil{
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            if let result = (try? JSONDecoder().decode([HintsStruct].self, from: data!))?.getValueArray() {
                DispatchQueue.main.async {
                    callback(result, nil)
                }
                return
            }
            DispatchQueue.main.async {
                callback(nil, nil)
            }
        }
        self.currentTask?.resume()
    }
    
}

import Foundation

class BaseRequest {
    lazy var urlComponents: URLComponents = {
        var componentsURL = URLComponents()
        componentsURL.scheme = "http"
        componentsURL.host = "rueo.ru"
        componentsURL.path = "/sercxo/"
        return componentsURL
    }()
    var currentTask: URLSessionDataTask?
    
    func isConnect() -> Bool {
        return Reachability.isConnectedToNetwork()
    }
}

import UIKit
import Foundation


class SearchRequest: BaseRequest {
    fileprivate var currentWord: String = ""
    
    func request(with word: String, callback: @escaping (String?, NSAttributedString?, Error?) throws -> Void) {
        guard
            let encodingString = word.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let url = URL(string: urlComponents.string! + encodingString)
        else {
            try? callback(nil, nil, nil)
            return
        }
        currentWord = word
        currentTask?.cancel()
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: TimeInterval(5))
        
        self.currentTask = URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            if let error = error,
                data == nil{

                DispatchQueue.main.async {
                    try? callback(nil, nil, error)
                }
                return
                
            }
            
            if let htmlString = data?.html2AttributedString,
                let sSelf = self {
                DispatchQueue.main.async {
                    try? callback(sSelf.currentWord, htmlString, nil)
                }
            }
            
        }
        self.currentTask?.resume()
    }
}

import UIKit
import Foundation


struct HistoryTableCellStruct {
    let title: String
}

class HistoryTableCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    
    func getWord() -> String{
        return titleLabel.text ?? ""
    }
}


extension HistoryTableCell: BaseTableViewCell{
    func configure(for object: Any?) {
        guard let cell = object as? HistoryTableCellStruct else { return }
        titleLabel.text = cell.title
    }
}

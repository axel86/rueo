import Foundation
import UIKit
import WebKit

protocol ResultSearchDelegate: class {
    func showAlert(alert: UIAlertController)
    func setAndRequest(word: String)
    func setRedColorSearchField()
    func setDefaultColorSearchField()
}

class ResultSearch: BaseView, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var textView: UITextView!{
        didSet{
            textView.text = ""
            textView.attributedText = NSAttributedString(string: "")
            textView.addKeyboardHandler()
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(onTapTextView(sender:)))
            tap.delegate = self
            textView.addGestureRecognizer(tap)
        }
    }
    private var sizeFont:CGFloat = 12{
        didSet{
            if sizeFont > 40 {
                sizeFont = 40
            }
            if sizeFont < 10 {
                sizeFont = 10
            }
            loadContent()
        }
    }
    
    weak var delegate: ResultSearchDelegate?
    
    private var attributedResult: NSAttributedString?
    private var currentWord: String?
    private let searchRequest: SearchRequest = SearchRequest()
    
    @objc func onTapTextView(sender: UITapGestureRecognizer){
        
        let myTextView = sender.view as! UITextView
        let layoutManager = myTextView.layoutManager
        
        var location = sender.location(in: myTextView)
        location.x -= myTextView.textContainerInset.left
        location.y -= myTextView.textContainerInset.top
        
        let characterIndex = layoutManager.characterIndex(for: location, in: myTextView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        if characterIndex < myTextView.textStorage.length {
            
            let attributtedValue = myTextView.attributedText.attribute(NSAttributedStringKey.myName, at: characterIndex, effectiveRange: nil) as? String
            if let value = attributtedValue?.deleteAccentuation() {
                delegate?.setAndRequest(word: value)
                History.shared.add(item: value)
            }
        }
        
    }
    
    
    func showAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alert.addAction(action)
        }
        
        delegate?.showAlert(alert: alert)
    }
    
    func load(word: String?) {
        currentWord = word
        guard let word = currentWord else {
            showAlert(title: "Error", message: "Not found word", actions: [
                UIAlertAction(title: "Ok", style: .default, handler: nil)
                ])
            return
        }
        hideLoading()
        showLoading()
        searchRequest.request(with: word) {[weak self] word, attrString, error in
            guard let sSelf = self else {
                return
            }
            sSelf.hideLoading()
            if let attrString = attrString, let word = word {
                do {
                    sSelf.attributedResult = try attrString.findResult(with: word)
                } catch ErrorResult.NoResult {
                   sSelf.delegate?.setRedColorSearchField()
                }
                sSelf.loadContent()
            } else if let error = error {
                sSelf.showAlert(title: "Error",
                                message: error.localizedDescription,
                                actions:
                    [
                        UIAlertAction(title: "Repeat", style: .default, handler: {_ in
                            sSelf.load(word: sSelf.currentWord)
                        }),
                        UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    ])
                } else{
                    sSelf.showAlert(title: "Error", message: "Pleas try again", actions: [
                    UIAlertAction(title: "Ok", style: .default, handler: nil)
                    ])
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        loadContent()
    }
    
    func loadContent() {
        guard let currentResult = attributedResult else { return }
        
        textView.attributedText = currentResult.getStringWith(size: sizeFont)
        delegate?.setDefaultColorSearchField()
    }
    @IBAction func handlePinch(recognizer: UIPinchGestureRecognizer){
        sizeFont += (recognizer.scale / 5) * recognizer.velocity
        recognizer.scale = 1
        loadContent()
    }
}

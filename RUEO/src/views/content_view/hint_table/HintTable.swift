import Foundation
import UIKit

protocol HintTableDelegate: class {
    func setAndRequest(word: String)
    func repeatRemoveLastChar(with word: String)
    func isEqualCurrentWord(for word: String)
}

class HintTable: BaseView {

    @IBOutlet weak var table: UITableView!{
        didSet{
            table.addKeyboardHandler()
        }
    }
    
    weak var delegate: HintTableDelegate?
    
    private var hints: [TableViewCellDescription] = []
    private var currentWord: String = ""
    private var hintRequest: HintRequest = HintRequest()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupTable()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTable()
    }
    
    
    func goHintRequest(with word: String) {
        currentWord = word
        self.hintRequest.request(with: word) {[weak self] strings, error in
            guard let sSelf = self else {return}

            sSelf.hints = []
            if let strings = strings {
                strings.forEach { hint in
                    sSelf.hints.append(
                        TableViewCellDescription(
                            cellType: HintTableViewCell.self,
                            height: UITableViewAutomaticDimension,
                            object: HintTableViewCellStruct(
                                textLabel: hint
                            )
                        )
                    )
                }
            }

            sSelf.table.reloadData()
            if sSelf.hints.isEmpty {
                sSelf.delegate?.repeatRemoveLastChar(with: sSelf.currentWord)
            }else{
                sSelf.delegate?.isEqualCurrentWord(for: sSelf.currentWord)
            }
        }
    }
}

extension HintTable: UITableViewDataSource, UITableViewDelegate {
    func setupTable(){
        table.delegate = self
        table.dataSource = self
        
        table.estimatedRowHeight = 35
        table.rowHeight = UITableViewAutomaticDimension
        
        table.register(HintTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.configureCell(with: self.hints[indexPath.row], for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? HintTableViewCell else { return }
        endEditing(true)
    
        delegate?.setAndRequest(word: cell.getWord())
        History.shared.add(item: cell.getWord())
        print(History.shared.history)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return hints[indexPath.row].height
    }
    
}



import Foundation
import UIKit

struct HintTableViewCellStruct {
    let textLabel: String
}

class HintTableViewCell: UITableViewCell {
    @IBOutlet weak var hintText: UILabel!
    static let rowHeight: CGFloat = 35

    func getWord() -> String {
        return hintText.text! 
    }
}

extension HintTableViewCell: BaseTableViewCell {
    func configure(for object: Any?) {
        guard let cell = object as? HintTableViewCellStruct else { return }
        self.hintText.text = cell.textLabel
    }
}

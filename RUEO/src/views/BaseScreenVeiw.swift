import Foundation
import UIKit

class BaseScreenView: UIView {
    
    // support of iOS 10 and early versions
    @IBOutlet weak var statusBarHeightConstraint: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateStatusBarHeightConstraint()
    }
    
    func onLoad() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onViewTapped))
        addGestureRecognizer(tap)
    }
    
    func onAppear() {
    }
    
    @objc func onViewTapped() {
        endEditing(true)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateStatusBarHeightConstraint()
    }
    
    func statusBarStyle() -> UIStatusBarStyle {
        return .lightContent
    }
    
    func updateStatusBarHeightConstraint() {
        
        if #available(iOS 11.0, *) {
            return
        }
        
        guard let statusBarHeightConstraint = statusBarHeightConstraint else { return }
        
        let newHeight: CGFloat
        
        if UIApplication.shared.isStatusBarHidden {
            newHeight = 0.0
        } else {
            newHeight = UIApplication.shared.statusBarFrame.size.height
        }
        
        if statusBarHeightConstraint.constant != newHeight {
            statusBarHeightConstraint.constant = newHeight
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
}

import Foundation
import UIKit

extension UITextField {
    func makeDefaultTextField(with placeholder: String) {
        autocapitalizationType = .words
        
        let str = NSAttributedString(string: placeholder, attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.white.withAlphaComponent(0.5),
            ])
        
        attributedPlaceholder = str
    }
}

import UIKit
import Foundation

extension UIStackView{
    func clear() {
        for arrengedSubview in arrangedSubviews{
            arrengedSubview.removeFromSuperview()
        }
    }
    
    func addArrangedSubview(_ views: [UIView]){
        for view in views {
            self.addArrangedSubview(view)
        }
    }
}

import Foundation
import UIKit

enum ErrorResult: Error{
    case NoResult
}

extension NSAttributedString {
    private func getMatchStringWithWord(word: String) -> String{
        return "(?<=\n" + word + ")(.|\n)+(?=Редакция)"
    }
    
    private var matchStringForDelete: String {
        return "Редакция(.|\n)+?\n\n"
    }
    
    private var matchWord:String {
        return "(?<=\\W)[\\w]+?(?=\\W)"
    }
    private func  getMyAttributteWith(word: String) -> [NSAttributedStringKey:Any] {
        return [NSAttributedStringKey.myName : word]
    }
    static var nameAttribute: String {
        return "MyCustomAttributeKey"
    }
    static var noResult: String{
        return "No Result"
    }
    
    func findResult(with word: String) throws -> NSAttributedString{
        guard
            let rangeAll = self.string.matches(for: getMatchStringWithWord(word: word)).first else
        {
            throw ErrorResult.NoResult
        }
        
        let resultAttributedString = NSMutableAttributedString(
            attributedString: self.attributedSubstring(from: rangeAll))
        var isEndRange = false
        while (!isEndRange){
            if let range = resultAttributedString.string.matches(for: matchStringForDelete).first {
                resultAttributedString.deleteCharacters(in: range)
            }else{
                isEndRange = true
            }
        }
        
        return resultAttributedString.addMyAttributted()
    }
    
    func getStringWith(size: CGFloat) -> NSAttributedString {
        let fullrange = NSRange(location: 0, length: self.length)
        let mutableAttributedString = NSMutableAttributedString(attributedString: self)
        mutableAttributedString.enumerateAttribute(NSAttributedStringKey.font, in: fullrange, options: .longestEffectiveRangeNotRequired) {
            (attribute: Any? , range: NSRange, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            if let attribute = attribute as? UIFont{
                let scaledFont = UIFont(descriptor: attribute.fontDescriptor, size: size)
                mutableAttributedString.addAttribute(NSAttributedStringKey.font, value: scaledFont, range: range)
            }
        }
        return mutableAttributedString.attributedSubstring(from: fullrange)
    }
    
    func addMyAttributted() -> NSAttributedString {
        let mutAttrStr = NSMutableAttributedString(attributedString: self)
        let ranges = mutAttrStr.string.matches(for: matchWord)
        for range in ranges {
            let word = (mutAttrStr.string as NSString).substring(with: range)
            mutAttrStr.addAttributes(getMyAttributteWith(word: word), range: range)
        }
        return mutAttrStr.attributedSubstring(from: NSRange(location: 0, length: mutAttrStr.length))
    }
}
extension NSAttributedStringKey {
    static let  myName = NSAttributedStringKey(rawValue: NSAttributedString.nameAttribute)
}

extension String{
    
    func deleteAccentuation() -> String{
        var res = ""
        for item in self.unicodeScalars{
            if (item.value != 769){
                res += item.description
            }
        }
        return res
    }
    
    func matches(for regex: String) -> [NSRange] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(location: 0, length: self.count))
            
            return results.compactMap { $0.range }

        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

import UIKit

extension UIViewController{
    static func viewControllerIdentifer() -> String{
        return String(describing: self)
    }
}

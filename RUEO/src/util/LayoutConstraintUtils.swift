import UIKit
import Foundation

extension NSLayoutConstraint{
    
    static var heightForSearchBar: CGFloat {
        return AppDelegate.isIphoneX() ? 100 : 76
    }
    
    static var lenghtToSuperView: CGFloat {
        return AppDelegate.isIphoneX() ? 52 : 28
    }
}

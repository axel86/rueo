import Foundation
import UIKit
import ObjectiveC

class ScrollViewKeyboardHandler {
    
    weak var scrollView: UIScrollView!
    let originalContentInset: UIEdgeInsets
    
    
    init(scrollView: UIScrollView) {
        self.scrollView = scrollView
        self.originalContentInset = scrollView.contentInset
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    var originalFrame: CGRect?
    var originalContentOffset: CGPoint = CGPoint(x: 0, y: 0)
    
    @objc func keyboardNotification(notification: NSNotification) {
        originalContentOffset = scrollView.contentOffset
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if let window = UIApplication.shared.delegate?.window! {
                if endFrame!.origin.y < window.frame.maxY {
                    if originalFrame == nil {
                        originalFrame = scrollView.frame
                    }
                    
                    let viewFrame = scrollView.superview!.convert(originalFrame!, to: scrollView.superview!)
                    let diff = endFrame!.height - (window.frame.maxY - viewFrame.maxY)
                    
                    UIView.animate(withDuration: duration,
                                   delay: TimeInterval(0),
                                   options: animationCurve,
                                   animations: { [weak self] in
                                    guard let strongSelf = self else { return }
                                    
                                    var contentInset = strongSelf.originalContentInset
                                    contentInset.bottom = diff + 50
                                    strongSelf.scrollView.contentInset = contentInset
                                    contentInset.bottom = diff
                                    strongSelf.scrollView.scrollIndicatorInsets =  contentInset
                        },
                                   completion: nil)
                } else {
                    originalFrame = nil
                    UIView.animate(withDuration: duration,
                                   delay: TimeInterval(0),
                                   options: animationCurve,
                                   animations: { [weak self] in
                                    guard let strongSelf = self else { return }
                                    
                                    strongSelf.scrollView.contentInset = strongSelf.originalContentInset
                                    strongSelf.scrollView.scrollIndicatorInsets =  strongSelf.originalContentInset
                        },
                                   completion: nil)
                }
            }
        }
    }
}

extension UIScrollView {
    
    private struct AssociatedKey {
        static var keyboardHandler = "keyboardHandler"
    }
    
    var keyboardHandler: ScrollViewKeyboardHandler? {
        get {
            return getAssociatedObject(object: self, associativeKey: &AssociatedKey.keyboardHandler)
        }
        
        set {
            if let value = newValue {
                setAssociatedObject(object: self, value: value, associativeKey: &AssociatedKey.keyboardHandler, policy: objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    func addKeyboardHandler() {
        if let _ = keyboardHandler {
            self.keyboardHandler = nil
        }
        
        self.keyboardHandler = ScrollViewKeyboardHandler(scrollView: self)
    }
}


import Foundation
import UIKit

struct TableViewCellDescription {
    let cellType: BaseTableViewCell.Type
    let height: CGFloat
    let object: Any?
}

extension UITableView {
    
    func register<T: BaseTableViewCell>(_ classType: T.Type) {
        register(UINib(nibName: classType.cellIdentifier(), bundle: nil), forCellReuseIdentifier: classType.cellIdentifier())
    }
    
    func configureCell(with cellDescription: TableViewCellDescription, for indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: cellDescription.cellType.cellIdentifier(), for: indexPath)
        if let baseCell = cell as? BaseTableViewCell {
            baseCell.configure(for: cellDescription.object)
        }
        
        return cell
    }
}

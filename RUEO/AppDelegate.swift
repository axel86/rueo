//
//  AppDelegate.swift
//  RUEO
//
//  Created by Алексей Лопух on 26.04.2018.
//  Copyright © 2018 Алексей Лопух. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    s
    class var shared: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        configureRealm()
        
        return true
    }
    
    func configureRealm() {
        var config = Realm.Configuration.defaultConfiguration
        config.schemaVersion = 0
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
        _ = try! Realm()
    }
    
    static func isIphoneX() -> Bool {
        let isTopGreatZero = ((UIApplication.shared.delegate as? AppDelegate)?.window?.safeAreaInsets.top ?? CGFloat(0)) > CGFloat(0)
        if #available(iOS 11, *){
            return isTopGreatZero
        }
        return false
    }
}

